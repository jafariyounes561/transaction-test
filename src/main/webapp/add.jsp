<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>Transaction Form</title>
    <style>
        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="text"], select {
            padding: 5px;
            border-radius: 3px;
            border: 1px solid #ccc;
            width: 250px;
            margin-bottom: 15px;
        }

        input[type="submit"] {
            background-color: #4CAF50;
            color: white;
            padding: 10px 20px;
            border-radius: 4px;
            border: none;
            cursor: pointer;
        }

        input[type="submit"]:hover {
            background-color: #45a049;
        }
    </style>
</head>
<body>

<div align="center">
    <h1>Transaction Form</h1>
    <form method="post" action="add/transaction">
        <label for="reference">Reference:</label>
        <input type="text" id="reference" name="reference" required>

        <label for="compteDeb">Compte Débit:</label>
        <input type="text" id="compteDeb" name="compteDeb" required>

        <label for="compteCred">Compte Crédit:</label>
        <input type="text" id="compteCred" name="compteCred" required>

        <label for="montant">Montant:</label>
        <input type="text" id="montant" name="montant" required>

        <label for="devise">Devise:</label>
        <select id="devise" name="devise" required>
            <option value="USD">USD</option>
            <option value="EUR">EUR</option>
            <option value="GBP">GBP</option>
            <option value="JPY">JPY</option>
        </select>
        <br>

        <input type="submit" value="Submit">
    </form>
</div>
</body>
</html>

