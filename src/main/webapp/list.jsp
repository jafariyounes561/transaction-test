<%@ page import="com.jafari.devoir.bean.Transaction" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>List des Transactions</title>
  <style>
    /* Set font family and size for the entire page */
    body {
      font-family: Arial, sans-serif;
      font-size: 16px;
    }

    /* Style the table */
    table {
      border-collapse: collapse;
      margin: 20px 0;
    }

    /* Style the table headers */
    th {
      background-color: #4CAF50;
      color: white;
      font-weight: bold;
      text-align: center;
      padding: 10px;
    }

    /* Style the table cells */
    td {
      text-align: center;
      padding: 8px;
    }

    /* Style the table rows */
    tr:nth-child(even) {
      background-color: #f2f2f2;
    }

    /* Style the "Add a Transaction" link */
    h2 a {
      color: #4CAF50;
      text-decoration: none;
    }

    /* Style the delete link */
    a[href^="delete/ref/"] {
      color: #f44336;
      text-decoration: none;
    }

    a[href^="delete/ref/"]:hover {
      text-decoration: underline;
    }

  </style>
</head>
<body>
<h1>List des Transactions</h1>

<table border="1" width="100%">
  <tr>
    <th>ID</th>
    <th>reference</th>
    <th>compteDeb</th>
    <th>compteCred</th>
    <th>montant</th>
    <th>devise</th>
    <th>type</th>
    <th>date</th>
    <th>delete</th>
  </tr>

<%
  List<Transaction> transactions = (List<Transaction>) request.getAttribute("transactions");
  for(Transaction transaction : transactions){%>
  <tr>
    <td align="center"><%= transaction.getId() %></td>
    <td align="center"><%= transaction.getReference()%></td>
    <td align="center"><%= transaction.getCompteDeb()%></td>
    <td align="center"><%= transaction.getCompteCred() %></td>
    <td align="center"><%= transaction.getMontante()%></td>
    <td align="center"><%= transaction.getDevise()%></td>
    <td align="center"><%= transaction.getType()%></td>
    <td align="center"><%= transaction.getDate()%></td>
    <td align="center"><a href="delete/ref/<%=transaction.getReference()%>">delete</a> </td>
  </tr>

  <%}%>
</table>

<h2><a href="add">Add a Transaction</a></h2>
</body>
</html>
