package com.jafari.devoir.dao;

import com.jafari.devoir.bean.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionDao extends JpaRepository<Transaction, Long> {

    Transaction findByReference(String reference);

    int deleteByReference(String reference);


}
