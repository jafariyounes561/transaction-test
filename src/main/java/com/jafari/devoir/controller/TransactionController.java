package com.jafari.devoir.controller;


import com.jafari.devoir.bean.Transaction;
import com.jafari.devoir.service.TransactionService;
import jakarta.annotation.security.RolesAllowed;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/")
public class TransactionController {

    @GetMapping("list/ref/{reference}")
    public Transaction findByReference(@PathVariable String reference) {
        return transactionService.findByReference(reference);
    }


    @GetMapping("delete/ref/{reference}")
    public void deleteByReference(@PathVariable String reference,HttpServletResponse response) throws IOException {
        transactionService.deleteByReference(reference);
        response.sendRedirect("/list");
    }


    @GetMapping("list")
    public ModelAndView findAll() {
        ModelAndView modelAndView = new ModelAndView();
        List<Transaction> transactions = transactionService.findAll();
        modelAndView.addObject("transactions",transactions);
        modelAndView.setViewName("list.jsp");
        return modelAndView;
    }

    @PostMapping("add/transaction")
    public void save(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Transaction transaction = new Transaction(request.getParameter("reference"),
                request.getParameter("compteDeb"),
                request.getParameter("compteCred"),
                request.getParameter("devise"),
                Double.parseDouble(request.getParameter("montant")));

        transactionService.save(transaction);
        response.sendRedirect("/list");
    }

    @GetMapping("add")
    public ModelAndView redirect() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("add.jsp");
        return modelAndView;

    }


    private TransactionService transactionService;
    @Autowired
    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }
}
