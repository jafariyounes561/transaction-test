package com.jafari.devoir.bean;


import jakarta.persistence.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Entity
@Table
public class Transaction {

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String type;
    private String reference;
    private String compteDeb;
    private String compteCred;
    private String devise;
    private String date;
    private double montante;

    public Transaction() {
    }

    public Transaction(Long id, String type, String reference, String compteDeb, String compteCred, String devise, String date, double montante) {
        this.id = id;
        this.type = type;
        this.reference = reference;
        this.compteDeb = compteDeb;
        this.compteCred = compteCred;
        this.devise = devise;
        this.date = date;
        this.montante = montante;
    }

    public Transaction(String reference, String compteDeb, String compteCred, String devise, double montante) {
        this.reference = reference;
        this.compteDeb = compteDeb;
        this.compteCred = compteCred;
        this.devise = devise;
        this.montante = montante;

        this.date = LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));

        if(compteCred.substring(0,3).equals(compteDeb.substring(0,3))){
            this.type="VIRINT";
        }else this.type="VIRCHAC";
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getCompteDeb() {
        return compteDeb;
    }

    public void setCompteDeb(String compteDeb) {
        this.compteDeb = compteDeb;
    }

    public String getCompteCred() {
        return compteCred;
    }

    public void setCompteCred(String compteCred) {
        this.compteCred = compteCred;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getMontante() {
        return montante;
    }

    public void setMontante(double montante) {
        this.montante = montante;
    }

    public boolean equals(Transaction transaction) {
        if (this.getReference().equals(transaction.getReference())){
            return true;
        }else return false;
    }
    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", reference='" + reference + '\'' +
                ", compteDeb='" + compteDeb + '\'' +
                ", compteCred='" + compteCred + '\'' +
                ", devise='" + devise + '\'' +
                ", date='" + date + '\'' +
                ", montante=" + montante +
                '}';
    }
}

