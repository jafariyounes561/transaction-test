package com.jafari.devoir.service;


import com.jafari.devoir.bean.Transaction;
import com.jafari.devoir.dao.TransactionDao;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionService {

    public Transaction findByReference(String reference) {
        return transactionDao.findByReference(reference);
    }

    @Transactional
    public int deleteByReference(String reference) {
        transactionDao.deleteByReference(reference);
        return 1;
    }

    public List<Transaction> findAll() {
        return transactionDao.findAll();
    }

    public int save(Transaction transaction) {
        if(findByReference(transaction.getReference())!=null){ return -1;}
         else {transactionDao.save(transaction);
         return 1;}
    }



    private TransactionDao transactionDao;
    @Autowired
    public TransactionService(TransactionDao transactionDao) {
        this.transactionDao = transactionDao;
    }


}
